package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/v8/linebot/insight"
	"github.com/line/line-bot-sdk-go/v8/linebot/messaging_api"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
}

func main() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": viper.Get("boss.kub"),
		})
	})
	r.GET("/send", sendMessage)
	r.GET("/get-aggregation", GetAggregationUnitNameList)
	r.GET("/get-statistics", GetStatisticsPerUnit)
	r.GET("/unit", unit)
	r.Run(":8081") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

func sendMessage(c *gin.Context) {
	url := "https://api.line.me/v2/bot/message/push"
	authToken := "{" + viper.GetString("authToken") + "}"

	data := map[string]interface{}{
		"to": viper.GetString("user"),
		"messages": []map[string]string{
			{
				"type": "text",
				"text": "Hello, world112333444",
			},
		},
		"customAggregationUnits": []string{
			"promotion_b",
		},
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("Error marshaling JSON:", err)
		return
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+authToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error making request:", err)
		return
	}
	defer resp.Body.Close()

	fmt.Println("Response status:", resp.Status)
	c.JSON(http.StatusOK, gin.H{
		"message": "sussecc",
	})
}

func GetAggregationUnitNameList(c *gin.Context) {

	url := "https://api.line.me/v2/bot/message/aggregation/list"
	authToken := "{" + viper.GetString("authToken") + "}"

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}

	req.Header.Set("Authorization", "Bearer "+authToken)

	q := req.URL.Query()
	limit := c.Query("limit")
	if limit != "" {
		q.Add("limit", limit)
		req.URL.RawQuery = q.Encode()
	}

	start := c.Query("start")
	if start != "" {
		q.Add("start", start)
		req.URL.RawQuery = q.Encode()
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request:", err)
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return
	}

	fmt.Println("Response Status:", resp.Status)
	fmt.Println("Response Body:", string(body))
	var bodyRes map[string]interface{}
	err = json.Unmarshal(body, &bodyRes)
	c.JSON(http.StatusOK, bodyRes)
}

func unit(c *gin.Context) {
	clientM, err := messaging_api.NewMessagingApiAPI(viper.GetString("authToken"))
	res, err := clientM.GetAggregationUnitNameList("30", "")
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("%v", res)
	c.JSON(http.StatusOK, res)
}

func GetStatisticsPerUnit(c *gin.Context) {

	client, err := insight.NewInsightAPI(viper.GetString("authToken"))
	if err != nil {
		fmt.Println(err)
	}
	customAggregationUnit := c.Query("customAggregationUnit")
	from := c.Query("from")
	to := c.Query("to")
	res, err := client.GetStatisticsPerUnit(customAggregationUnit, from, to)
	c.JSON(http.StatusOK, res)
}
