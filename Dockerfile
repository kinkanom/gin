FROM golang:1.22 AS builder

WORKDIR /app

EXPOSE 8081

COPY . .

RUN go mod tidy

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o goapp main.go

#######################################

FROM alpine:3.14

WORKDIR /app

COPY --from=builder /app/goapp .

COPY --from=builder /app/config.yaml .

COPY --from=builder . .

ENTRYPOINT [ "/app/goapp" ]
